package be.kdg.prog12.m4.examples;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.Optional;

public class TestWindowEvent extends Application {
    @Override
    public void start(Stage stage) {
        stage.setOnCloseRequest(event -> {
            final Alert sure = new Alert(Alert.AlertType.CONFIRMATION);
            sure.setHeaderText("Are you sure?");
            sure.setContentText("Are you sure you want to exit?");

            // This is needed on Linux due to a bug: https://bugs.openjdk.java.net/browse/JDK-8179073
            sure.setResizable(true);
            sure.onShownProperty().addListener(e -> {
                Platform.runLater(() -> sure.setResizable(false));
            });
            // End of bug workaround

            Optional<ButtonType> choice = sure.showAndWait();
            if (choice.get().getText().equalsIgnoreCase("CANCEL")) {
                event.consume();
            }
        });

        /*
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                final Alert sure = new Alert(Alert.AlertType.CONFIRMATION);
                sure.setHeaderText("Are you sure?");
                sure.setContentText("Are you sure you want to exit?");
                Optional<ButtonType> choice = sure.showAndWait();
                if (choice.get().getText().equalsIgnoreCase("CANCEL")) {
                    event.consume();
                }
            }
        });
        */
        stage.setTitle("Confirm");
        stage.setScene(new Scene(new BorderPane()));
        stage.show();
    }
}
