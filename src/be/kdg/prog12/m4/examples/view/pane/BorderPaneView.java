package be.kdg.prog12.m4.examples.view.pane;

import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

public class BorderPaneView extends BorderPane {
    private TextArea area;
    private MenuItem exit;
    private Label status;

    public BorderPaneView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.area = new TextArea();
        this.exit = new MenuItem("Exit");
        this.status = new Label("Waiting for text.");
        this.status.setStyle("-fx-background-color: orange");
    }

    private void layoutNodes() {
        Menu bestandMenu = new Menu("File", null, exit);
        MenuBar menuBar = new MenuBar(bestandMenu);
        this.setCenter(area);
        this.setTop(menuBar);
        this.setBottom(status);
        status.setMaxWidth(Double.MAX_VALUE);
    }
}
