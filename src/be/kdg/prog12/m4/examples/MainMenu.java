package be.kdg.prog12.m4.examples;

import be.kdg.prog12.m4.examples.view.MainMenuPresenter;
import be.kdg.prog12.m4.examples.view.MainMenuView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
//editted by Joseph
public class MainMenu extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        MainMenuView view = new MainMenuView();
        MainMenuPresenter presenter = new MainMenuPresenter(view);
        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.setTitle("Examples M4");
        stage.show();
    }
}
